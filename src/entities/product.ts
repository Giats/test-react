export interface IProduct {
  product: string;
  cost: number;
  image: string;
  profit: number;
  our_price: number;
  min_price: number;
  srp: number;
  no_competitors: number;
  last_update: Date;
}
