export interface ICompetitor {
  name: string;
  image: string;
  stores: string[];
  // Status should probably an enum when we learn the types
  status: string;
  products: number;
  average_position: number;
  average_price: number;
  store_link: string;
  skroutz_link: string;
}
