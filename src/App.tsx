import React, { Fragment } from 'react';
import { useDispatch } from 'react-redux';
import { useRoutes } from 'react-router-dom';

import AlertComponent from './components/helper-components/AlertComponent';
import {
  triggerError,
  triggerInfo,
  triggerSuccess,
} from './redux/alert/actions';
import routes from './routes';

function App() {
  // Construct and consume all routes with their routing
  const routing = useRoutes(routes);

  const dispatch = useDispatch();

  return (
    <>
      {routing}

      {/* ALERT COMPONENT IF IN NEED TO TRIGGER AN ALERT JSUT DISPATCH  
      example: dispatch(triggerError("error test"))
      */}
      <AlertComponent />
    </>
  );
}

export default App;
