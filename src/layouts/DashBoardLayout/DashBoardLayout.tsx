import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Link, Navigate, Outlet } from 'react-router-dom';

import CssBaseline from '@material-ui/core/CssBaseline';

import { Drawer } from '../../components/ui-kit/drawer/drawer';
import { InboxIcon, MailIcon } from '../../components/ui-kit/icons/icons';
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '../../components/ui-kit/list/list';
import { Toolbar } from '../../components/ui-kit/toolbar/toolbar';
import { AppState } from '../../redux/reducers';
import { NavBar } from './components/NavBar';
import { useStyles } from './dashboard.styles';

const DashBoardLayout = () => {
  const classes = useStyles();

  // Using the Dasboard layout as a wrapper for the auth,
  // if the Redux state is authed it shows the app/ dir which is for the authed user.

  const { isAuthenticated } = useSelector((state: AppState) => state.user);

  if (!isAuthenticated) {
    return <Navigate to="/" />;
  }
  return (
    <div className={classes.root}>
      <CssBaseline />

      {/* Appbar Component */}
      <div className={classes.appBar}>
        <NavBar />
      </div>

      <Drawer
        classes={{
          paper: classes.drawerPaper,
        }}
        className={classes.drawer}
        variant="permanent">
        <Toolbar />
        <div className={classes.drawerContainer}>
          <List>
            {[
              'Products',
              'Competitors',
              'Tools',
              'Configurations',
              'Report',
            ].map((text, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <Link key={index} to={`/app/${text.toLowerCase()}`}>
                <ListItem button key={text}>
                  <ListItemIcon>
                    {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                  </ListItemIcon>
                  <ListItemText primary={text} />
                </ListItem>
              </Link>
            ))}
          </List>
        </div>
      </Drawer>
      <main className={classes.content}>
        <Toolbar />
        <Outlet />
      </main>
    </div>
  );
};

export { DashBoardLayout };
