import React from 'react';
import { useSelector } from 'react-redux';
import { Link, Navigate, Outlet } from 'react-router-dom';

import { AppState } from '../../redux/reducers';

export const PublicLayout = () => {
  const { isAuthenticated } = useSelector((state: AppState) => state.user);

  // Using the Dasboard layout as a wrapper for the auth,
  // if the Redux state is NOT authed it shows the / dir which is for the unauhtenticated user.
  if (isAuthenticated) {
    return <Navigate to="app/" />;
  }
  return (
    <div>
      <main>
        {/* Outlet consumes the route */}
        <Outlet />
      </main>
    </div>
  );
};
