import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';

export default MuiAlert;

export type { AlertProps };
