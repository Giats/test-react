import React from 'react';
import { useParams } from 'react-router-dom';

interface Props {}

export const ProductPage = (props: Props) => {
  const { id } = useParams();
  return <div>Unique Product page- {id}</div>;
};
