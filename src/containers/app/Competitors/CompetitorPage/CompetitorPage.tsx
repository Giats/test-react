import React from 'react';
import { useParams } from 'react-router-dom';

interface Props {}

export const CompetitorPage = (props: Props) => {
  const { id } = useParams();
  return <div>Competitor - {id}</div>;
};
