import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';

import { Avatar } from '../../../components/ui-kit/avatar/avatar';
import { Button } from '../../../components/ui-kit/button/button';
import { Container } from '../../../components/ui-kit/container/container';
import { Grid } from '../../../components/ui-kit/grid/grid';
import { AccountCircle } from '../../../components/ui-kit/icons/icons';
import { TextField } from '../../../components/ui-kit/textfield/textfield';
import { Typography } from '../../../components/ui-kit/typography/typography';
import { triggerError } from '../../../redux/alert/actions';
import { triggerSignIn } from '../../../redux/user/actions';
import { loginUser } from '../../../services/authApi';
import { useStyles } from './login.styles';

type Inputs = {
  email: string;
  password: string;
};

const Login = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { handleSubmit, control } = useForm<Inputs>();

  const onSubmit = async (data: Inputs) => {
    // TODO
    // 1. Make api call to login
    // 2. If success for auth, se authentication to tru and set token to local storage.

    // try {
    //   const response = await loginUser(data.email, data.password);
    //   // If response set token and state to isAuthenticated
    //   if (response) {
    //     dispatch(triggerSignIn("dummy token"));
    //   }
    // } catch (error) {
    //   // put the error message here
    //   dispatch(triggerError(`${error.message}`));
    // }

    dispatch(triggerSignIn('dummy token'));
  };

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <AccountCircle />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>

        <form
          className={classes.form}
          noValidate
          onSubmit={handleSubmit(onSubmit)}>
          <Controller
            as={TextField}
            className={classes.input}
            control={control}
            label="Email"
            name="email"
            size="small"
            type="email"
            variant="outlined"
          />
          <Controller
            as={TextField}
            className={classes.input}
            control={control}
            label="Password"
            name="password"
            size="small"
            type="password"
            variant="outlined"
          />
          <Button
            className={classes.submit}
            color="primary"
            fullWidth
            type="submit"
            variant="contained">
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link to="/">Forgot password?</Link>
            </Grid>
            <Grid item>
              <Link to="/">Sign Up</Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
};

export { Login };
