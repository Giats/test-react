import { combineReducers } from 'redux';

import Alert from './alert/reducer';
import User from './user/reducer';

const allReducer = combineReducers({
  user: User,
  alert: Alert,
});
export default allReducer;

export type AppState = ReturnType<typeof allReducer>;
