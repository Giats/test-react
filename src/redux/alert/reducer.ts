import { ActionTypes, AlertActionTypes } from './actions';

interface IAlertState {
  open: boolean;
  message: string;
  type: ActionTypes;
}

const initialState: IAlertState = {
  open: false,
  message: '',
  type: ActionTypes.CLOSE,
};
// FUNCTIONS TO BE CALLED ON REDUCER.

const triggerErrorAlert = (state: IAlertState, message: string) => {
  return {
    open: true,
    message,
    type: ActionTypes.ERROR,
  };
};

const triggerWarningAlert = (state: IAlertState, message: string) => {
  return {
    open: true,
    message,
    type: ActionTypes.WARNING,
  };
};

const triggerSuccessAlert = (state: IAlertState, message: string) => {
  return {
    open: true,
    message,
    type: ActionTypes.SUCCESS,
  };
};
const triggerInfoAlert = (state: IAlertState, message: string) => {
  return {
    open: true,
    message,
    type: ActionTypes.INFO,
  };
};

const triggerCloseAlert = () => {
  return initialState;
};

const alertReducer = (state = initialState, action: AlertActionTypes) => {
  switch (action.type) {
    case ActionTypes.ERROR: {
      return triggerErrorAlert(state, action.payload);
    }
    case ActionTypes.WARNING: {
      return triggerWarningAlert(state, action.payload);
    }
    case ActionTypes.SUCCESS: {
      return triggerSuccessAlert(state, action.payload);
    }
    case ActionTypes.INFO: {
      return triggerInfoAlert(state, action.payload);
    }
    case ActionTypes.CLOSE: {
      return triggerCloseAlert();
    }
    default:
      return state;
  }
};

export default alertReducer;
