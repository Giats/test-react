import React from 'react';
import { Navigate } from 'react-router-dom';

import { Competitors } from './containers/app/Competitors';
import { CompetitorPage } from './containers/app/Competitors/CompetitorPage';
import { Configurations } from './containers/app/Configurations';
import { Home } from './containers/app/Home';
import { Products } from './containers/app/Products';
import { ProductPage } from './containers/app/Products/ProductPage';
import { Report } from './containers/app/Report';
import { Tools } from './containers/app/Tools';
import { Login } from './containers/public/Login';
import NotFound from './containers/public/NotFound/NotFound';
import { DashBoardLayout } from './layouts/DashBoardLayout';
import { PublicLayout } from './layouts/PublicLayout';

// path -- the base path of the components
// element -- the wrapepr component use it for layout - auth checking
// children -- route elements, you put the path ex. app/[child.path] and renders the element prop.

// TODO wrap the dashboard Layout to an auth component that checks
// if user.isAuthenticated same with public IF we want then to be exclusive

const routes = [
  {
    path: 'app',
    element: <DashBoardLayout />,
    children: [
      { path: '/', element: <Home /> },
      { path: '/products', element: <Products /> },
      { path: '/products/:id', element: <ProductPage /> },
      { path: '/competitors', element: <Competitors /> },
      { path: '/competitors/:id', element: <CompetitorPage /> },
      { path: '/tools', element: <Tools /> },
      { path: '/configurations', element: <Configurations /> },
      { path: '/report', element: <Report /> },
      { path: '*', element: <Navigate to="/404" /> },
    ],
  },
  {
    path: '/',
    element: <PublicLayout />,
    children: [
      { path: '/', element: <Navigate to="/login" /> },
      { path: '/login', element: <Login /> },
      { path: '/*', element: <Navigate to="/404" /> },
      { path: '404', element: <NotFound /> },
    ],
  },
];

export default routes;
