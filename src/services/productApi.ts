import { IProduct } from '../entities/product';
import { API } from '../helpers/api';
import { cookieStore } from '../helpers/storage';

const getProducts = async (): Promise<IProduct[]> => {
  const api = new API({
    headers: {
      Authorization: cookieStore.get('token') || '',
    },
  });
  const response = await api.get(`products`);
  return response;
};

const getProductById = async (id: number): Promise<IProduct> => {
  const api = new API({
    headers: {
      Authorization: cookieStore.get('token') || '',
    },
  });
  const response = await api.get(`products/${id}`);
  return response;
};

const editProductById = async (id: number, data: any): Promise<any> => {
  const api = new API({
    headers: {
      Authorization: cookieStore.get('token') || '',
    },
  });
  const response = await api.put(`products/${id}`, data);
  return response;
};

// TODO typecheck here
const deleteProductById = async (id: number): Promise<any[]> => {
  const api = new API({
    headers: {
      Authorization: cookieStore.get('token') || '',
    },
  });
  const response = await api.delete(`products/${id}`);
  return response;
};

// TODO typecheck here
const createProduct = async (id: number, data: any): Promise<any[]> => {
  const api = new API({
    headers: {
      Authorization: cookieStore.get('token') || '',
    },
  });
  const response = await api.post(`products/${id}`, { data });
  return response;
};

export {
  getProducts,
  getProductById,
  editProductById,
  deleteProductById,
  createProduct,
};
