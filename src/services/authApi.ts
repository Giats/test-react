import { IUser } from '../entities/user';
import { API } from '../helpers/api';
import { cookieStore } from '../helpers/storage';

const loginUser = async (email: string, password: string) => {
  const api = new API({
    baseURL: process.env.REACT_APP_SERVER_BASEURL,
  });
  const response = await api.post(`auth`, {
    email,
    password,
  });
  return response;
};

export { loginUser };
