import { ICompetitor } from '../entities/competitor';
import { API } from '../helpers/api';
import { cookieStore } from '../helpers/storage';

const getCompetitors = async (): Promise<ICompetitor[]> => {
  const api = new API({
    headers: {
      Authorization: cookieStore.get('token') || '',
    },
  });
  const response = await api.get(`competitors`);
  return response;
};

const getCompetitorById = async (id: number): Promise<ICompetitor> => {
  const api = new API({
    headers: {
      Authorization: cookieStore.get('token') || '',
    },
  });
  const response = await api.get(`competitors/${id}`);
  return response;
};

const editCompetitorById = async (id: number, data: any): Promise<any> => {
  const api = new API({
    headers: {
      Authorization: cookieStore.get('token') || '',
    },
  });
  const response = await api.put(`competitors/${id}`, data);
  return response;
};

// TODO typecheck here
const deleteCompetitorById = async (id: number): Promise<any[]> => {
  const api = new API({
    headers: {
      Authorization: cookieStore.get('token') || '',
    },
  });
  const response = await api.delete(`competitors/${id}`);
  return response;
};

// TODO typecheck here
const createCompetitor = async (id: number, data: any): Promise<any[]> => {
  const api = new API({
    headers: {
      Authorization: cookieStore.get('token') || '',
    },
  });
  const response = await api.post(`competitors/${id}`, { data });
  return response;
};

export {
  getCompetitors,
  getCompetitorById,
  editCompetitorById,
  deleteCompetitorById,
  createCompetitor,
};
